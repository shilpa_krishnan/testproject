import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  form: any = FormGroup;
  dataSource: any;
  tableHeaders: any;
  newArray: any[] = [];
  currentUser: any;
  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.currentUser = localStorage.getItem('user')
    this.form = this.fb.group({
      search: []
    })
    this.getTableData();
  }

  getTableData(){
    this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe(res => {
      this.dataSource = res;
      this.tableHeaders = this.dataSource.length > 0 ? Object.keys(this.dataSource[0]) : [];
    });
  }

  search(data: any) {
    // this.newArray = this.dataSource
    if (data !== '') {
      this.newArray = this.dataSource.filter(function (ele: any, i: any, array: any) {
        let arrayelement = ele.body.toLowerCase() || ele.title.toLowerCase()
        return arrayelement.includes(data)
      })
    this.dataSource = this.newArray
    } else if(data === ''){
      this.getTableData();
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['../../']);
  }
}
