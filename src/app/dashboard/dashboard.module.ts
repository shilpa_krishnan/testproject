import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { InputsModule } from '../inputs/inputs.module';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  declarations: [
    DashboardComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    InputsModule
  ],
  exports: [
    InputsModule
  ]
})
export class DashboardModule { }
