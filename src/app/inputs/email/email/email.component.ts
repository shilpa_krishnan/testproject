import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  myForm : any = FormGroup;
  @Output() email: EventEmitter<string> = new EventEmitter<string>();
  constructor(public fb:FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      email:['']
    })
  }

  emailValue(event: any){
   if(event.target.value !== '' && event.target.value !== null && event.target.value !== undefined){
    this.email.emit(event.target.value)
   }
   if((!event.target.value.includes('@') || !event.target.value.includes('.') || !event.target.value.includes('com')) && event.target.value!==''){
     this.myForm.get('email').setErrors({'invalid' : true});
   }
  }

}
