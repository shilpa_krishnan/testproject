import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormControlName } from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {
  myForm : any = FormGroup;
  hide:boolean = false;
  @Output() password: EventEmitter<string> = new EventEmitter<string>();
  constructor(public fb:FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      password:['']
    })
  }
  passwordValue(event: any){
    if(event.target.value !== '' && event.target.value !== null && event.target.value !== undefined){
     this.password.emit(event.target.value)
    }
   }
}
