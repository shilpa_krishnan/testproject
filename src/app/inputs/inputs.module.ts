import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailComponent } from './email/email/email.component';
import { PasswordComponent } from './password/password/password.component';
import { AngularMaterialModule } from '../material.module';
import { TableComponent } from './table/table.component';

@NgModule({
  declarations: [
    EmailComponent,
    PasswordComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [
    EmailComponent,
    PasswordComponent,
    TableComponent,
    AngularMaterialModule
  ]
})
export class InputsModule { }
