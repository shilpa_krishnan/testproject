import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from 'src/app/dashboard/dialog/dialog.component';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements AfterViewInit {
  form: any = FormGroup;
  pageSize = 10;
  @Input() dataSource:any;
  @Input() columnDefs: any[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort: MatSort = new MatSort;
  @Output() searchKey = new EventEmitter<String>();
  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      search: []
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  get displayedColumns() {
    return this.columnDefs;
  }

  getRecord(row : any){
   console.log(row)
   const dialogRef = this.dialog.open(DialogComponent, {
    height: '40%',
    data: row,
    disableClose: true
  });
  }

  search() {
    this.searchKey.emit(this.form.get('search').value)
}
}
