import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login.service';
import { HttpClient } from '@angular/common/http';
import data from '../../../_files/data.json';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userEmail: any;
  userPassword: any;
  show: boolean = false;
  constructor(
    public loginService: LoginService,
    public http: HttpClient,
    private router: Router) { }
  public userList: { name: string, username: string, password: string, email: string }[] = data;
  ngOnInit(): void {
  }
  login() {
    this.router.navigate(['../../dashboard']);
  }
  loginPassword(event: any) {
    this.userPassword = event;
    if (this.userList.find(x => x.email === this.userEmail && x.password === this.userPassword)) {
      let currentUser = this.userList.find(x => x.email === this.userEmail && x.password === this.userPassword)
      localStorage.setItem('user', currentUser?.name as string);
      this.show = true;
    }else{
      this.show = false;
    }

  }
  loginEmail(event: any) {
    this.userEmail = event;
  }

}
